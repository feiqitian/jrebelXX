package com.ddatsh;

import com.zeroturnaround.licensing.*;

import java.io.*;
import java.util.*;

public class KeyManager {
	private String loc = "/Volumes/RamDisk/";
	public KeyManager() {
	}
	public KeyManager(String loc){
		this.loc=loc;
	}
	public void writeLic(Map<String, String> lic) throws IOException{
       	UserLicense license = new UserLicense();
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bao);
        oos.writeObject(lic);
        oos.close();
        license.setLicense(bao.toByteArray());
        license.setSignature(StringUtils.gs(license.getLicense()));
        oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(loc+"jrebel.lic")));
        oos.writeObject(license);
        oos.close();
	}

}
