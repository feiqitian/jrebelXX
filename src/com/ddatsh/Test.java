package com.ddatsh;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;

import com.zeroturnaround.licensing.UserLicense;

public class Test {
	public static void main(String[] args) {
		try {
			ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream
                    ("/Volumes/RamDisk/rebel.license")));
			Object obj = ois.readObject();
			ois.close();
			if ((obj != null) && (obj instanceof UserLicense)) {
				UserLicense lic = (UserLicense) obj;
				System.out.println(StringUtils.vs(lic.getLicense(), lic.getSignature()));
				ois = new ObjectInputStream(new ByteArrayInputStream(lic.getLicense()));
				Map localMap = (Map) ois.readObject();
                System.out.println(localMap.keySet().size());
                ois.close();
				localMap.put("commercial", "true");
				ByteArrayOutputStream bao = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(bao);
				oos.writeObject(localMap);
				oos.close();
				lic.setLicense(bao.toByteArray());
				lic.setSignature(StringUtils.gs(lic.getLicense()));
				//oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("c:\\javarebel.new.lic")
				// ));
				/*	oos.writeObject(lic);
					oos.close();*/
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
