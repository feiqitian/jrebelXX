package com.ddatsh;
import java.io.*;
import java.security.*;
import java.util.*;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class MainWindow {
	protected Shell shell;
	public static void main(String[] args) {
		try {
			MainWindow window = new MainWindow();
			window.open();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void open() {
		final Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	protected void createContents() {
		shell = new Shell();
		shell.setSize(453, 224);
		shell.setText("JavaRebel Keygen");
		final Group licenceInformationGroup = new Group(shell, SWT.NONE);
		licenceInformationGroup.setText("Licence Information");
		licenceInformationGroup.setBounds(0, 0, 445, 197);
		final Text txtName = new Text(licenceInformationGroup, SWT.BORDER);
		txtName.setText("zhangthe9");
		txtName.setBounds(92, 25, 327, 25);
		final Label nameLabel = new Label(licenceInformationGroup, SWT.NONE);
		nameLabel.setAlignment(SWT.RIGHT);
		nameLabel.setText("Name");
		nameLabel.setBounds(32, 28, 54, 22);
		final Label nameLabel_1 = new Label(licenceInformationGroup, SWT.NONE);
		nameLabel_1.setBounds(10, 70, 77, 22);
		nameLabel_1.setAlignment(SWT.RIGHT);
		nameLabel_1.setText("Organization");
		final Text txtOrg = new Text(licenceInformationGroup, SWT.BORDER);
		txtOrg.setText("zhangthe9");
		txtOrg.setBounds(93, 67, 327, 25);
		final Label nameLabel_1_1 = new Label(licenceInformationGroup, SWT.NONE);
		nameLabel_1_1.setBounds(10, 113, 77, 22);
		nameLabel_1_1.setAlignment(SWT.RIGHT);
		nameLabel_1_1.setText("Comment");
		final Text txtComment = new Text(licenceInformationGroup, SWT.BORDER);
		txtComment.setText("");
		txtComment.setBounds(93, 110, 327, 25);
		final Button generateButton = new Button(licenceInformationGroup, SWT.NONE);
		generateButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				MessageBox mb = new MessageBox(shell);
				try {
					writeLic(txtName.getText(), txtOrg.getText(), txtComment.getText());
					mb.setMessage("License File Generated Successfully!");
				}
				catch (Exception e1) {
					mb.setMessage(e1.toString());
				}
				finally {
					mb.open();
				}
			}
		});
		generateButton.setText("Generate");
		generateButton.setBounds(173, 154, 93, 23);
		//
	}

    private static byte[] a(Serializable paramSerializable) {
        try {
            ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream localObjectOutputStream = new ObjectOutputStream(localByteArrayOutputStream);
            localObjectOutputStream.writeObject(paramSerializable);
            localObjectOutputStream.flush();
            return localByteArrayOutputStream.toByteArray();
        } catch (IOException localIOException) {
        }
        return null;
    }
	private void writeLic(String user, String org, String com) throws Exception {
		Map lic = new HashMap(16);
		lic.put("commercial", "true");
		lic.put("override", "true");
        lic.put("Seats", "Unlimited");
		lic.put("Product", "JavaRebel");
		lic.put("Name", user);
		lic.put("Organization", org);
        lic.put("OffLineLease","true");
        //lic.put("Enterprise", "true");
		lic.put("Comment", com);
        Date d=new Date();
        lic.put("start-date",d);

        MessageDigest localMessageDigest = MessageDigest.getInstance("SHA");
        localMessageDigest.update(a(d));
        lic.put("digest",localMessageDigest.digest());
		KeyManager km = new KeyManager();
		km.writeLic(lic);
	}
}
